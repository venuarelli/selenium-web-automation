package com.google.chat.test.personalchathome;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.google.chat.test.BaseTest;
import com.selenium.automation.util.logger.LogManager;
import com.selenium.automation.util.verify.Verify;

public class PersonalChatTest extends BaseTest {
    /**
     * {@link LogManager}
     */
    LogManager logger = LogManager.getLogger(PersonalChatTest.class);

    String message = RandomStringUtils.randomAlphanumeric(5);
    String editMessage = RandomStringUtils.randomNumeric(5);

    @Test
    public void test1VerifyPersonalChat() {

        final boolean isPersonalChatOpen = personalChatHomePage.isPersonalChatOpen(personName);

        logger.logTestVerificationStep("Personal chat page is opened. ");
        Verify.verifyTrue(isPersonalChatOpen,
                "Personal chat home page should be open, but its not.");

    }

    @Test
    public void test2VerifyChatMessage() {

        logger.logTestStep("Enter chat message:  " + message);
        personalChatHomePage.enterAndSendMessage(message);
        final boolean isSendChatMessageDisplayed =
                personalChatHomePage.isSendChatMessageDisplayed(message);

        logger.logTestVerificationStep("Verify chat message is displayed.");
        Verify.verifyTrue(isSendChatMessageDisplayed, "chat message " + message
                + " should be displayed, but it is  not.");
    }

    @Test
    public void test3VerifyEditChatMessage() {
        message = RandomStringUtils.randomAlphabetic(6);
        logger.logTestStep("Enter chat message:  " + message);
        personalChatHomePage.enterAndSendMessage(message);
        personalChatHomePage.editChatMessage(message, editMessage);
        final boolean isEditedTextDisplayed = personalChatHomePage.isEditedTextDisplayed();

        logger.logTestVerificationStep("Verify edited text is displayed.");
        Verify.verifyTrue(isEditedTextDisplayed, "Edited text should be displayed but it is not.");

        final boolean isSendChatMessageDisplayed =
                personalChatHomePage.isSendChatMessageDisplayed(message + " " + editMessage);

        logger.logTestVerificationStep("Verify edited chat message is displayed.");
        Verify.verifyTrue(isSendChatMessageDisplayed, "edited chat message " + message
                + " should be displayed, but it is  not.");
    }

    @Test
    public void test4VerifyDeleteMassage() {
        message = RandomStringUtils.randomNumeric(6);
        personalChatHomePage.enterAndSendMessage(message);
        personalChatHomePage.deleteMessage(message);
        final boolean isDeletedMessageDisplayed =
                personalChatHomePage.isDeletedMessageDisplayed(message);
        logger.logTestVerificationStep("Verify delete message is invisible: " + message);
        Verify.verifyTrue(isDeletedMessageDisplayed, "deleted message " + message
                + " is invisible, but it is not.");

    }

    @Test
    public void test5VerifyToggleHistory() {
        personalChatHomePage.toggleHistoryOn();
        final boolean isHistoryOnMessageDisplayed =
                personalChatHomePage.isHistoryOnMessageDisplayed(userName);
        logger.logTestStep("Verify History on message displayed: " + isHistoryOnMessageDisplayed);

        Verify.verifyTrue(isHistoryOnMessageDisplayed,
                "History on message should be displayed, but it is not.");

        personalChatHomePage.toggleHistoryOff();

        final boolean isHistoryOffMessageDisplayed =
                personalChatHomePage.isHistoryOffMessageDisplayed();

        logger.logTestStep("Verify History on message displayed: " + isHistoryOffMessageDisplayed);
        Verify.verifyTrue(isHistoryOnMessageDisplayed,
                "History off message should be displayed, but it is not.");

    }

    @Test
    public void test6VerifyUploadImage() {
        personalChatHomePage.clickOnUploadButton();
        personalChatHomePage.uploadImage();
        personalChatHomePage.clickOnSendMessageButton();
    }
}
