package com.google.chat.test;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.google.chat.pages.chat_home.ChatHomePage;
import com.google.chat.pages.login.LoginPage;
import com.google.chat.pages.personalchat_home.PersonalChatHomePage;
import com.selenium.automation.ui.implementation.Browser;
import com.selenium.automation.util.enums.BrowserType;
import com.selenium.automation.util.logger.LogManager;
import com.selenium.automation.util.reader.PropertyReader;

public class BaseTest {

    protected Browser browser;

    final LogManager logger = LogManager.getLogger(BaseTest.class);
    protected String userName;

    protected ChatHomePage chatHomePage;
    protected PersonalChatHomePage personalChatHomePage;

    protected String personName;

    @BeforeClass
    public void openBrowser() {
        browser = new Browser();
        final LoginPage loginPage = new LoginPage(browser);
        chatHomePage = new ChatHomePage(browser);
        personalChatHomePage = new PersonalChatHomePage(browser);

        final String url = PropertyReader.getProperty("url");
        browser.openPage(url, "windows", BrowserType.CHROME);

        userName = PropertyReader.getProperty("userName");
        logger.logSetupStep("Enter user name: " + userName);
        loginPage.enterUserNmae(userName);

        logger.logSetupStep("Click on next button.");
        loginPage.clickOnNextButton();

        final String password = PropertyReader.getProperty("password");

        logger.logSetupStep("Enter password.");
        loginPage.enterPassword(password);

        logger.logSetupStep("Click on submit button.");
        loginPage.clickOnSubmitButton();

        personName = PropertyReader.getProperty("person_name");

        logger.logSetupStep("Enter person name.");
        chatHomePage.enterPersonName(personName);

        logger.logSetupStep("Click on suggest person name: " + personName);
        chatHomePage.clickOnSuggestName(personName);

    }

    @AfterClass
    public void signout() {
        logger.logTearDownStep("Click on chat more button.");
        chatHomePage.clickOnChatMoreButton(personName);
        browser.getWaits().sleep(2);
        logger.logTearDownStep("Click on hide conversation button.");
        chatHomePage.clickOnHideConversation();

        logger.logTearDownStep("Click on sign out.");
        chatHomePage.clickOnSignOut();
    }
}
