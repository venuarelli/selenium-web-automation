package com.google.chat.pages.removeaccount;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.google.chat.pages.BasePage;
import com.selenium.automation.ui.implementation.Browser;
import com.selenium.automation.ui.implementation.Waits;

public class RemoveAccountPage extends BasePage {

    Waits waits;

    public RemoveAccountPage(Browser browser) {
        super(browser);
        PageFactory.initElements(browser.getDriver(), this);
        waits = new Waits();
    }

    public void clickOnRemoveAccount() {
        waits.findClickable(By.xpath("//p[text()='Signed out']")).click();
    }

    public void clickOnRemoveAccountButton() {
        waits.findClickable(By.xpath("//button[text()='Remove an account']")).click();

    }
}
