package com.google.chat.pages.chat_home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.google.chat.pages.BasePage;
import com.google.chat.pages.personalchat_home.PersonalChatHomePage;
import com.selenium.automation.ui.implementation.Browser;
import com.selenium.automation.ui.implementation.Waits;
import com.selenium.automation.util.enums.LocatorType;
import com.selenium.automation.util.logger.LogManager;

public class ChatHomePage extends BasePage {
    Waits waits;

    LogManager logger = LogManager.getLogger(ChatHomePage.class);

    public ChatHomePage(Browser browser) {
        super(browser);
        waits = browser.getWaits();
        // waits.inVisibilityofElement(By.xpath("//div[text()='Loading...']/../.."));
        // if (waits.inVisibilityofElement(By.xpath("//div[text()='Loading...']/../.."))) {
        // System.out.println("Loading is invisible =====.");
        // }
        PageFactory.initElements(browser.getDriver(), this);
    }

    public void clickOnChatMoreButton(String personName) {

        waits.findClickable(By.xpath("(//span[text()='" + personName + "'])[2]"));

        browser.getMouseHandler().moveToElement(LocatorType.XPATH,
                "(//span[text()='" + personName + "'])[2]");

        browser.getWaits().findClickable(
                By.xpath("//div[contains(@aria-label,'Options for " + personName + "')]"));

        browser.getClick().click(LocatorType.XPATH,
                "//div[contains(@aria-label,'Options for " + personName + "')]");
    }

    public void clickOnHideConversation() {

        browser.getWaits().findClickable(By.xpath("//content[@aria-label='Hide direct message']"));

        browser.getClick().click(LocatorType.XPATH, "//content[@aria-label='Hide direct message']");
    }

    public void clickOnSignOut() {
        waits.findClickable(By.xpath("//a[contains(@href,'SignOutOptions')]")).click();

        logger.logInfo("Click on signout.");
        waits.findClickable(By.linkText("Sign out")).click();

    }

    public void clickOnStarButton() {
        browser.getClick().click(LocatorType.XPATH, "//content[@aria-label='Star group']']");
    }

    public PersonalChatHomePage clickOnSuggestName(String personName) {
        final String xpathExpression = "//span[contains(text(),'" + personName + "')]";
        browser.getWaits().findClickable(By.xpath(xpathExpression));

        browser.getClick().click(LocatorType.XPATH, xpathExpression);
        return new PersonalChatHomePage(browser);
    }

    public void clickOnTextField() {
        browser.getClick().click(LocatorType.XPATH, "//div[text()='Find people, rooms, bots']");
    }

    public void clickOnUnStarButton() {
        browser.getClick().click(LocatorType.XPATH, "//content[@aria-label='Unstar group']");
    }

    public void enterPersonName(String personName) {

        /*
         * waitForLoadingPopup();
         * waits.inVisibilityofElement(By.xpath("//div[text()='Loading...']"));
         * waitForLoadingPopup();
         * waits.inVisibilityofElement(By.xpath("//div[text()='Loading...']"));
         */
        waits.sleep(15);

        browser.getWaits().findClickable(By.xpath("//div[text()='Find people, rooms, bots']"));

        browser.getClick().click(LocatorType.XPATH, "//div[text()='Find people, rooms, bots']");

        browser.getWaits().findClickable(
                By.xpath("//input[contains(@aria-label,'Type person, room')]"));

        browser.getTextField().setTextField(LocatorType.XPATH,
                "//input[contains(@aria-label,'Type person, room')]", personName);
    }

    public boolean isHideConversationPopupDisplayed(String personName) {
        final String xpath = "(//div[contains(text(),'Hid message with')])[2]";
        System.out.println(xpath);
        final WebElement element = waits.findVisibilityElement(By.xpath(xpath));
        final boolean isDisplayed = element.isDisplayed();
        logger.logInfo("Is Hide conversation pop-up displayed:  " + isDisplayed);
        return isDisplayed;
    }

    public boolean waitForLoadingPopup() {
        final WebElement element =
                waits.findVisibilityElement(By.xpath("//div[text()='Loading...']"));
        final boolean displayed = element.isDisplayed();
        logger.logInfo("Is Loading pop-up displayed:  " + displayed);
        return displayed;
    }

}
