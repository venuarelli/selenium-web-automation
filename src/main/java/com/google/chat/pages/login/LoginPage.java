package com.google.chat.pages.login;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.google.chat.pages.BasePage;
import com.google.chat.pages.chat_home.ChatHomePage;
import com.selenium.automation.ui.implementation.Browser;
import com.selenium.automation.util.enums.LocatorType;

public class LoginPage extends BasePage {

    public LoginPage(Browser browser) {
        super(browser);
        PageFactory.initElements(browser.getDriver(), this);
    }

    public void clickOnNextButton() {
        browser.getClick().click(LocatorType.XPATH, "//span[text()='Next']");
    }

    public ChatHomePage clickOnSubmitButton() {
        browser.getWaits().findClickable(By.xpath("//span[text()='Next']"));

        browser.getClick().click(LocatorType.XPATH, "//span[text()='Next']");
        return new ChatHomePage(browser);
    }

    public void enterPassword(String password) {
        browser.getWaits().findClickable(By.name("password"));
        browser.getTextField().setTextField(LocatorType.NAME, "password", password);
    }

    public void enterUserNmae(String userName) {
        browser.getTextField().setTextField(LocatorType.ID, "identifierId", userName);
    }
}
