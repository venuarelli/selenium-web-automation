package com.google.chat.pages.personalchat_home;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.google.chat.pages.BasePage;
import com.selenium.automation.ui.implementation.Browser;
import com.selenium.automation.util.enums.LocatorType;
import com.selenium.automation.util.logger.LogManager;
import com.selenium.automation.util.reader.PropertyReader;

public class PersonalChatHomePage extends BasePage {

    LogManager logger = LogManager.getLogger(PersonalChatHomePage.class);

    public PersonalChatHomePage(Browser browser) {
        super(browser);
        PageFactory.initElements(browser.getDriver(), this);
    }

    public void clickOnSendMessageButton() {
        browser.getWaits().sleep(6);
        waitUntilUploadButtonVisible();
        browser.getWaits().findClickable(By.xpath("(//div[@title='Send Message'])[last()]"))
        .click();
    }

    public void clickOnUploadButton() {
        browser.getWaits().findClickable(By.xpath("//div[@title='Upload file']")).click();
    }

    public void deleteMessage(String message) {

        final String xpathExpression = "//div[text()='" + message + "']";
        final WebElement element = browser.getWaits().findClickable(By.xpath(xpathExpression));

        browser.getMouseHandler().moveToElement(LocatorType.XPATH, xpathExpression);
        // browser.getWaits().findClickable(By.xpath(xpathExpression)).click();
        browser.getWaits().findClickable(By.xpath("(//div[@data-tooltip='Delete'])[last()]"))
        .click();
        logger.logInfo("Click on delete button.");
        browser.getWaits().findClickable(By.xpath("(//div[@aria-label='Delete'])[last()]")).click();
    }

    public void editChatMessage(String message, String editMessage) {
        final String xpathExpression = "//div[text()='" + message + "']";
        final WebElement element = browser.getWaits().findClickable(By.xpath(xpathExpression));

        browser.getMouseHandler().moveToElement(LocatorType.XPATH, xpathExpression);

        final String editChatXpath = "(//div[@aria-label='Edit message'])[last()]";
        browser.getWaits().findClickable(By.xpath(editChatXpath)).click();
        browser.getTextField().setTextField(LocatorType.XPATH, editChatXpath, " " + editMessage);

        logger.logInfo("Click on update button.");
        browser.getWaits().findClickable(By.xpath("//span[text()='Update']")).click();

    }

    public void enterAndSendMessage(String message) {
        final WebElement element =
                browser.getWaits().findClickable(
                        By.xpath("//div[@role='textbox' and contains(@aria-label,'Message')]"));
        logger.logInfo("Click on message text box. ");
        element.click();
        element.sendKeys(message);
        element.sendKeys(Keys.ENTER);
    }

    public boolean isDeletedMessageDisplayed(String message) {
        final boolean isDisplayed =
                browser.getWaits().inVisibilityofElement(
                        By.xpath("//div[text()='" + message + "']"));

        logger.logInfo("Element is not visible.");
        return isDisplayed;
    }

    public boolean isEditedTextDisplayed() {
        final WebElement element =
                browser.getWaits().findVisibilityElement(
                        By.xpath("(//span[text()='Edited'])[last()]"));
        final boolean isDisplayed = isDisplayed(element);

        logger.logInfo("Is edited text dispalyed:  " + isDisplayed);

        return isDisplayed;

    }

    public boolean isHistoryOffMessageDisplayed() {
        final WebElement element =
                browser.getWaits().findVisibilityElement(
                        By.xpath("//div[text()='Messages deleted after 24 hours']"));
        final boolean isDispalyed = isDisplayed(element);
        logger.logInfo("Is history on message displayed: " + isDispalyed);

        return isDispalyed;
    }

    public boolean isHistoryOnMessageDisplayed(String userName) {
        final WebElement element =
                browser.getWaits().findVisibilityElement(
                        By.xpath("//div[text()='Message Umadevi Jeediguntla']"));
        final boolean isDispalyed = isDisplayed(element);
        logger.logInfo("Is history on message displayed: " + isDispalyed);

        return isDispalyed;
    }

    public boolean isPersonalChatOpen(String personName) {
        browser.getWaits().findVisibilityElement(
                By.xpath("(//span[text()='" + personName + "'])[3]"));

        final boolean isDisplayed =
                isDisplayed(browser.getFinder().findElementByXPath(
                        "(//span[text()='" + personName + "'])[3]"));

        logger.logInfo("Ispersonal chat homepage opened: " + isDisplayed);

        return isDisplayed;
    }

    public boolean isSendChatMessageDisplayed(String message) {
        final WebElement element =
                browser.getWaits().findVisibilityElement(
                        By.xpath("//div[text()='" + message + "']"));
        final boolean isDisplayed = element.isDisplayed();

        logger.logInfo("Is send chat message displayed.  " + isDisplayed);
        return isDisplayed;

    }

    public void toggleHistoryOff() {
        final WebElement element =
                browser.getWaits().findClickable(By.xpath("//div[@title='Toggle history']"));
        final String attributeValue = element.getAttribute("aria-checked");
        if (attributeValue.equals("false")) {
            logger.logInfo("Click on history off button.");
            element.click();
        }
    }

    public void toggleHistoryOn() {
        final WebElement element =
                browser.getWaits().findClickable(By.xpath("//div[@title='Toggle history']"));
        final String attributeValue = element.getAttribute("aria-checked");
        if (attributeValue.equals("true")) {
            logger.logInfo("Click on history on button.");
            element.click();
        }
    }

    public void uploadImage() {
        final String imagePath = PropertyReader.getProperty("imagePath");
        logger.logInfo("Image path:  " + imagePath);

        final String projrctDirectoryPath = System.getProperty("user.dir");
        logger.logInfo("Project directory path:  " + projrctDirectoryPath);

        final String totalPath = projrctDirectoryPath + imagePath;
        logger.logInfo("Total path:  " + totalPath);

        uploadImage(projrctDirectoryPath + imagePath);
    }

    public void waitUntilUploadButtonVisible() {
        browser.getWaits().findClickable(By.xpath("//div[@title='Upload file']"));
    }
}
