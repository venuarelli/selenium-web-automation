package com.google.chat.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import com.selenium.automation.ui.implementation.Browser;
import com.selenium.automation.util.logger.LogManager;

public class BasePage {
    public Browser browser;

    LogManager logger = LogManager.getLogger(BasePage.class);

    public BasePage(Browser browser) {
        this.browser = browser;
    }

    public boolean isDisplayed(By by) {
        boolean isDisplayed = false;
        try {
            isDisplayed = browser.getDriver().findElement(by).isDisplayed();
        } catch (final NoSuchElementException nse) {
            logger.logError("Element not displayed. " + nse.getMessage());
            return false;
        }
        return isDisplayed;

    }

    public boolean isDisplayed(WebElement element) {
        boolean isDisplayed = false;
        try {
            isDisplayed = element.isDisplayed();
        } catch (final NoSuchElementException nse) {
            logger.logError("Element not displayed. " + nse.getMessage());
            return false;
        }
        return isDisplayed;

    }

    public void uploadImage(String imagePath) {
        final StringSelection stringSelection = new StringSelection(imagePath);
        final Clipboard clipBoard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipBoard.setContents(stringSelection, null);

        try {
            final Robot robot = new Robot();
            robot.delay(3000);
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_CONTROL);

            robot.delay(500);

            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);

        } catch (final AWTException e) {
            e.printStackTrace();
        }
    }
}